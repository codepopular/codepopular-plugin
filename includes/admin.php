<?php 
namespace CodePopular;
class Admin{
	public  function __construct(){
		$addressbook = new Admin\Addressbook();
		new Admin\menu( $addressbook );
		$this->admin_actions( $addressbook );		
	}


	public function admin_actions( $addressbook ){
		add_action('admin_init', [$addressbook, 'form_handaler']);
		add_action('admin_post_codepopular_delete_address', [$addressbook, 'delete_address']);

	}

}

?>