<?php
namespace CodePopular\Admin;

use CodePopular\Traits\Form_Error;

class Addressbook
{

    use Form_Error;
    public function addressbook_plugin_page()
    {
        $action = isset($_GET['action']) ? $_GET['action'] : 'list';
        $id     = isset($_GET['action']) ? intval($_GET['id']) : 0;
        switch ($action) {
            case 'new':
                $template = __DIR__ . '/view/address-new.php';
                break;

            case 'edit':
                $address  = codepopular_get_edit_address($id);
                $template = __DIR__ . '/view/address-edit.php';
                break;

            case 'view':
                $template = __DIR__ . '/view/address-view.php';
                break;

            default:
                $template = __DIR__ . '/view/address-list.php';
                break;
        }

        if (file_exists($template)) {
            include $template;
        }

    }

    public function form_handaler()
    {
        if (!isset($_POST['address-submit'])) {
            return false;
        }

        if (!wp_verify_nonce($_POST['_wpnonce'], 'codepopular_address_nonce')) {
            wp_die('are you cheating');
        }

        if (!current_user_can('manage_options')) {
            wp_die('are you cheating');
        }
        $id    = isset($_POST['id']) ? intval($_POST['id']) : 0;
        $name  = isset($_POST['name']) ? sanitize_text_field($_POST['name']) : '';
        $email = isset($_POST['email']) ? sanitize_text_field($_POST['email']) : '';
        $phone = isset($_POST['phone']) ? sanitize_text_field($_POST['phone']) : '';
        $phone = isset($_POST['phone']) ? sanitize_text_field($_POST['phone']) : '';
        $age   = isset($_POST['age']) ? sanitize_text_field($_POST['age']) : '';

        // name validation
        if (empty($name)) {
            $this->errors['name'] = __('please provide a name', 'codepopular');
        }

        // email validation
        if (empty($email)) {
            $this->errors['email'] = __('please provide a email', 'codepopular');
        }

        // Phone validation
        if (empty($phone)) {
            $this->errors['phone'] = __('please provide a phone', 'codepopular');
        }
        // Age validation
        if (empty($age)) {
            $this->errors['age'] = __('please provide a age', 'codepopular');
        }

        if (!empty($this->errors)) {
            return;
        }

        $args = [

            'name'       => $name,
            'email'      => $email,
            'phone'      => $phone,
            'age'        => $age,
            'created_by' => get_current_user_id(),
        ];

        if ($id) {
            $args['id'] = $id;
        }

        $insert_id = cp_insert_address($args);

        if (is_wp_error($insert_id)) {
            // thow  error
            wp_die($insert_id->get_error_message());
        }

        if ($id) {
            wp_redirect(admin_url("admin.php?page=codepopular-slug&action=edit&id=".$id."&address-updated=true"));
        } else {

            $redirect_to = admin_url('admin.php?page=codepopular-slug&inserted=true', 'admin');
            wp_redirect($redirect_to);

        }

    }

    public function delete_address(){

        if (! wp_verify_nonce($_REQUEST['_wpnonce'], 'codepopular_delete_address')) {
            wp_die('are you cheating');
        }

        if (! current_user_can('manage_options')) {
            wp_die('are you cheating');
        }

         $id     = isset($_REQUEST['action']) ? intval($_REQUEST['id']) : 0;

         if(codepopular_delete_single_entity($id)){
             $redirect_to = admin_url('admin.php?page=codepopular-slug&deleted=true');
         }else{

             $redirect_to = admin_url('admin.php?page=codepopular-slug&deleted=false');
         }
         wp_redirect( $redirect_to );

    }

}
