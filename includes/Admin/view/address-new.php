<div class="wrap">
 <h1 class="inline-heading">New Address</h1>
<div class="container-fluid">
	<div class="row">
	<div class="col-md-9">
		<div class="row justify-content-center">

			<?php $name  = $this->has_error('name') ? $this->get_error('name') : '';?>
			<?php $email = $this->has_error('email') ? $this->get_error('email') : '';?>
			<?php $phone = $this->has_error('phone') ? $this->get_error('phone') : '';?>
			<?php $age   = $this->has_error('age') ? $this->get_error('age') : '';?>

			<div class="col-md-8">
				<form action="#" method="post">
				  <div class="form-group">
				    <label for="name">Name</label>
				    <input type="text" class="form-control" id="name" placeholder="Enter Your Name" name="name">

				    <span class="text-danger"><?php echo $name; ?></span>
				  </div>


				  <div class="form-group">
				    <label for="email">Email</label>
				    <input type="email" class="form-control" id="email" placeholder="Enter Your Email"  name="email">
				     <span class="text-danger"><?php echo $email; ?></span>
				  </div>



				  <div class="form-group">
				    <label for="phone">Phone</label>
				    <input type="text" class="form-control" id="phone" placeholder="Enter Your Phone" name="phone">
				      <span class="text-danger"><?php echo $phone; ?></span>
				  </div>



				  <div class="form-group">
				    <label for="age">Age</label>
				    <input type="text" class="form-control" id="age" placeholder="Enter Your Age" name="age">
				      <span class="text-danger"><?php echo $age; ?></span>
				  </div>


				  <?php wp_nonce_field('codepopular_address_nonce');?>
					<div class="form-group">
						<?php submit_button(__('Submit', 'codepopular'), 'form-control primary', 'address-submit', true);?>


			  		</div>
				</form>
			</div>
		</div>

		</div>
		<div class="col-md-3" style="background: red">

			dfgdfg
		</div>
	</div>
</div>



</div>




