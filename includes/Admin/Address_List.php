<?php
namespace CodePopular\Admin;

if (!class_exists('WP_List_Table')) {
    require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

/**
 * EXTENDS WP LIST TABLE
 */
class Address_List extends \WP_List_Table
{

    public function __construct()
    {
        parent::__construct([
            'singular' => 'contact',
            'plural'   => 'contacts',
            'ajax'     => false,

        ]);
    }

    public function get_columns()
    {
        return [

            'cb'         => '<input type="checkbox"/>',
            'name'       => __('name', 'codepopular'),
            'email'      => __('email', 'codepopular'),
            'phone'      => __('phone', 'codepopular'),
            'age'        => __('age', 'codepopular'),
            'created_at' => __('date', 'codepopular'),
        ];
    }

    public function get_sortable_columns()
    {

        $sortable_column = [
            'name'       => ['name', true],
            'created_at' => ['created_at', true],

        ];

        return $sortable_column;

    }

    protected function column_default($item, $column_name)
    {

        switch ($column_name) {
            case 'value':
                # code...
                break;

            default:
                return isset($item->$column_name) ? $item->$column_name : '';

        }
    }

    public function column_name($item)
    {
        $actions         = [];
        $actions['edit'] = sprintf('<a href="%s" title="%s">%s</a>', admin_url('admin.php?page=codepopular-slug&action=edit&id=' . $item->id), $item->id, __('Edit', 'codepopular'), __('Edit', 'codepopular'));

        $actions['delete'] = sprintf('<a href="%s" class="submitdelete" title="%s">%s</a>', wp_nonce_url(admin_url('admin-post.php?action=codepopular_delete_address&id=' . $item->id), 'codepopular_delete_address'), $item->id, __('Delete', 'codepopular'), __('Delete', 'codepopular'));

        return sprintf(
            '<a href="%1$s"><strong>%2$s</strong></a> %3$s', admin_url('admin.php?page=codepopular-slug&action=view&id=' . $item->id), $item->name, $this->row_actions($actions)
        );

    }

    public function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" value="%d" name="address_id[]" ', $item->id
        );
    }

    public function prepare_items()
    {
        $column = $this->get_columns();
        $hidden = [];

        $sortable = $this->get_sortable_columns();

        $per_page = 20;

        $this->_column_headers = [$column, $hidden, $sortable];

        $args = [
            'number' => $per_page,
            'offset' => $offset,

        ];

        if (isset($_REQUEST['orderby']) & isset($_REQUEST['order'])) {
            $args['orderby'] = $_REQUEST['orderby'];
            $args['order']   = $_REQUEST['order'];
        }

        $per_page     = 2;
        $current_page = $this->pagenum();
        $offset       = ($current_page - 1) * $per_page;

        $this->items = codepopular_get_address($args);
        $this->set_pagination_args([
            'total_items' => codepopular_address_count(),
            'per_page'    => $per_page,
        ]);

    }

}
