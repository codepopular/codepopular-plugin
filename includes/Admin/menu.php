<?php


namespace CodePopular\Admin;
class menu{
	public $addressbook;
	public function __construct( $addressbook ){
		$this->addressbook = $addressbook;
		add_action('admin_menu', [$this, 'codepopular_menu_page']);
    }


    public function codepopular_menu_page(){
    	$parent_slug = "codepopular-slug";
    	$capability = "manage_options";
    	$hook = add_menu_page(
			__('CodePopular title', 'codepopular'),
			__('CodePopular', 'codepopular'),
			$capability,
			$parent_slug,
			[$this->addressbook, 'addressbook_plugin_page']
		);
		add_submenu_page(
			$parent_slug,
			__('AddressBook', 'codepopular'),
			__('AddressBook', 'codepopular'),
			$capability,
			$parent_slug,
			[$this->addressbook, 'addressbook_plugin_page']
		);

		add_submenu_page(
			$parent_slug,
			__('settings', 'codepopular'),
			__('settings', 'codepopular'),
			$capability,
			'codepopular_settings',
			[$this, 'codepopular_settings']
		);

		add_action('admin_head-' . $hook, [$this, 'enqueue_assets']);
	}






	public function codepopular_settings(){
		echo "settings";
	}


	public function enqueue_assets(){
		wp_enqueue_style( 'codepopular_bootstrap' );
		
	}

}