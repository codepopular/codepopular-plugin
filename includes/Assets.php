<?php

namespace CodePopular;

class Assets
{
    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueue_assets']);
        add_action('admin_enqueue_scripts', [$this, 'enqueue_assets']);
    }

    public function get_scripts()
    {
        return [
            // frontend script
            'codepopular_forntend_script' => [
                'src'     => codpeopular_asset_url . '/frontend/js/frontend.js',
                'version' => filemtime(codepopular_plugin_path . '/assets/frontend/js/frontend.js'),
                'deps'    => ['jquery'],
            ],

            // backend script

        ];

    }

    public function get_styles()
    {
        return [

            // frontend style
            'codepopular_frontend_style' => [
                'src'     => codpeopular_asset_url . '/frontend/css/frontend.css',
                'version' => filemtime(codepopular_plugin_path . '/assets/frontend/css/frontend.css'),
            ], 

            // backend style
            'codepopular_bootstrap' => [
                'src'     => codpeopular_asset_url . '/backend/css/bootstrap.min.css',
                'version' => filemtime(codepopular_plugin_path . '/assets/backend/css/bootstrap.min.css'),
            ]

        ];
    }

    public function enqueue_assets()
    {

        $get_scripts = $this->get_scripts();
        foreach ($get_scripts as $handle => $script) {
            $deps = isset($script['deps']) ? $script['deps'] : false;
            wp_register_script($handle, $script['src'], $deps, $script['version'], true);
        }

        $get_styles = $this->get_styles();
        foreach ($get_styles as $handle => $style) {
            $deps = isset($style['deps']) ? $style['deps'] : false;
            wp_register_style($handle, $style['src'], $deps, $style['version']);
        }

        wp_localize_script( 'codepopular_forntend_script', 'codepopularAjax', [

            'ajaxurl' => admin_url('admin-ajax.php'),
            'error' => __('something went wrong', 'codepopular'),

        ]);

    }

}
