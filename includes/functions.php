<?php
/**
 * Address Insert
 * @param  array  $args [description]
 * @return [type]       [description]
 */
function cp_insert_address($args = [])
{
    global $wpdb;

    $defaults = [
        'name'       => '',
        'email'      => '',
        'phone'      => '',
        'age'        => '',
        'created_by' => get_current_user_id(),
        'created_at' => current_time('mysql'),
    ];
    $data = wp_parse_args($args, $defaults);

    if (isset($data['id'])) {
        $id = $data['id'];
        unset($data['id']);

        $updated = $wpdb->update(
            "{$wpdb->prefix}cp_address_books",
            $data,
            ['id' => $id],
            [
                '%s',
                '%s',
                '%s',
                '%d',
                '%d',
                '%s',
            ],
            ['%d'],

        );

        return $updated;

    } else {

        if (empty($data['name'])) {
            return new WP_Error('no-name', __('You must provide a name', 'codepopular'));
        }

        $inserted = $wpdb->insert(
            "{$wpdb->prefix}cp_address_books",
            $data,
            [
                '%s',
                '%s',
                '%s',
                '%d',
                '%d',
                '%s',
            ]
        );

        if (!$inserted) {
            return new \WP_Error('faild-to-insert', __('Faild to insert', 'codepopular'));
        }

        return $wpdb->insert_id;

    }

}

function codepopular_get_address($args = [])
{
    global $wpdb;
    $defaults = [
        'number'  => 20,
        'offset'  => 0,
        'orderby' => 'id',
        'order'   => 'ASC',

    ];

    $args = wp_parse_args($args, $defaults);

    $items = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT * FROM {$wpdb->prefix}cp_address_books
         ORDER BY {$args['orderby']} {$args['order']}
         LIMIT %d, %d",
            $args['offset'], $args['number']

        ));

    return $items;

}

/**
 * Adress Count
 * @return int
 */
function codepopular_address_count()
{
    global $wpdb;
    return (int) $wpdb->get_var("SELECT count(id) FROM {$wpdb->prefix}cp_address_books");

}

/**
 * Edit Single Entry
 */

function codepopular_get_edit_address($id)
{

    global $wpdb;
    return $wpdb->get_row(

        $wpdb->prepare(" SELECT * FROM {$wpdb->prefix}cp_address_books WHERE id = %d ", $id)

    );
}

/**
 * Delete Single Address
 */

function codepopular_delete_single_entity($id)
{
    global $wpdb;
    return $wpdb->delete(
        $wpdb->prefix . 'cp_address_books',
        ['id' => $id],
        ['%d']
    );
}
