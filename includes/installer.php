<?php 
namespace CodePopular;
class Installer{

	public function run(){
		$this->add_version();
		$this->create_table();
	}

	public function add_version(){
		 $installed = get_option('codepopular_first_installled_date');
         if( ! $installed){
         update_option('codepopular_first_installled_date', time());
          }
         update_option('codepopular_version', '1.0');
	}


	public function create_table(){


		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();


		$adress_table_scema = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}cp_address_books` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		  `name` varchar(255) NOT NULL,
		  `email` varchar(255) NOT NULL,
		  `phone` int(255) NOT NULL,
		  `age` int(255) NOT NULL,
		  `created_by` int(11) NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
		  `updated_at` timestamp NULL DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) $charset_collate";


		if(! function_exists('dbDelta')){
			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		}

		dbDelta($adress_table_scema);

	}

}


?>