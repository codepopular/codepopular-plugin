<?php 
/**
 * Plugin Name:       CodePopular
 * Plugin URI:        https://codepopular.com
 * Description:       Handle the basics with this plugin.
 * Version:           1.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Shamim
 * Author URI:        https://www.facebook.com/codershamim
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       codepopular

*/

if(! defined('ABSPATH')){
	exit();
}

require_once __DIR__ . '/vendor/autoload.php';


 final class CodePopular{

   const version = '1.0';

   private function __construct(){
     $this->define_constance();
     register_activation_hook(__FILE__, [$this, 'activate']);
     add_action('plugins_loaded', [$this, 'init_plugin']);
  }



  /**
   * plugin init
   */
  
  public static function init(){

    static $instance = false;
    if( ! $instance ){
      $instance = new self();
    }
    return $instance;
  }

/**
 * [define_constance description]
 * @return [type] [description]
 */
  public static function define_constance(){
    define('codepopuular_plugin_version', self::version);
    define('codepopular_file_path', __FILE__);
    define('codepopular_plugin_path', __DIR__);
    define('codepopular_plugin_url', plugins_url('', codepopular_file_path));
    define('codpeopular_asset_url', codepopular_plugin_url.'/assets');
   }



  /**
   * intialize plugin
   */

  public static function init_plugin(){
     new CodePopular\Assets;




     
      if( is_admin() ){
         new  CodePopular\Admin();
      }else{
         new  CodePopular\Frontend();
         
      }
   
  }

   /**
    * plugin activation
    */

  public static function activate(){

    $installer = new CodePopular\Installer();

    $installer->run();
     


   }


}



/**
 * [CodePopular description]
 */
function CodePopular(){
  return CodePopular::init();
}

/**
 * Call Main class funciton
 */

  CodePopular();

?>